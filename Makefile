all: es en
.PHONY: help
.DEFAULT_GOAL := all

en es: curriculum.tex ## Compilar en idioma español (es) o inglés (en), segun objetivo
	mtxrun --script context --purgeall -mode=$@ -result=curriculum-$@.pdf curriculum.tex

clean: ## Limpiar basura
	rm *.aux *.log *.tuc *.vis.*

help: ## Esta ayuda
	@awk -F ':|##' '/^[^\t].+?:.*?##/ { printf "\033[36m%-10s\033[0m %s\n", $$1, $$NF }' $(MAKEFILE_LIST)
